#!/usr/bin/perl

use DBI;
use URI::Escape;
use Text::vCard::Addressbook;

my $contacts = {};
my $names = {};



#### load all contacts into hash #### 
# id -> name
# or
# name -> name (remove duplicates)

$db = DBI->connect("dbi:SQLite:dbname=contacts2.db","",""); # Подключаемся к базе данных. Если файла users.db не существует, то он будет создан автоматически

my $query = $db->prepare("SELECT _id,display_name FROM raw_contacts");
$query->execute() or die($db->errstr);

while (@user = $query->fetchrow_array() ){
    $names->{$user[0]} = $user[1];
    
    # by name or by id
    $contacts->{ $names->{$user[0]} }{fullname} = $user[1];
    #$contacts->{ $user[0] }{fullname} = $user[1];
}

#### load contacts data #### 

load_contact_data();
#print Dumper $contacts;

$db->disconnect;

#### generate vcard addressbook

my $ab    = Text::vCard::Addressbook->new();
#$ab->set_encoding('utf-8');
#$ab->set_encoding('utf-8;encoding=quoted-printable');

foreach my $id (keys $contacts) {   
    my $vcard = $ab->add_vcard;
    
    $vcard->version( "3.0" );    
    $vcard->fullname( $contacts->{$id}{fullname} );
    
    ### node "moniker"
    my $p = $vcard->add_node( { 'node_type' => 'moniker', } );
    $p->family( $contacts->{$id}{name_full}[0]{data3} );
    $p->given( $contacts->{$id}{name_full}[0]{data2} );
    
    ### node "email"
    $vcard->EMAIL($contacts->{$id}{email_v2});
    
    ### node "birthday"
    $vcard->BDAY($contacts->{$id}{contact_event});
    
    ### node "phone"
    foreach my $phone ( @{$contacts->{$id}{phone_v2}} ) {
        
        $phone =~ s/^[78]([0-9]{10})$/+7$1/;
        $phone =~ s/^([0-9]{7})$/+7495$1/;
        
        my $p = $vcard->add_node( { 'node_type' => 'TEL', } );
        $p->value( $phone );
    }
    
    #$vcard->NOTE($contacts->{$id}{note});
    #$vcard->NOTE($contacts->{$id}{im});
    
    ### node "note"
    foreach my $v ( @{$contacts->{$id}{note}} ) {
        $v = custom_encode_string($v);
            
        my $p = $vcard->add_node( { 'node_type' => 'NOTE', } );
        $p->value( $v );
    }
    
    ### node "im"
    foreach my $v ( @{$contacts->{$id}{im}} ) {
        $v = custom_encode_string($v);
            
        my $p = $vcard->add_node( { 'node_type' => 'NOTE', } );
        $p->value( $v );
    }
    
    ### node "organization"
    foreach my $v ( @{$contacts->{$id}{organization_full}} ) {
        my $org;
        foreach my $vv ( keys %{$v}) {
            if($v->{$vv}) {
                $org .= " " . $v->{$vv};
            }
        }
        $org =~ s/^[\t\f ]+|[\t\f ]+$//mg;
        
        if($org && $org ne "") {
            $org = custom_encode_string($org);
            
            my $p = $vcard->add_node( { 'node_type' => 'ORG', } );
            $p->name( $org );
        }
    }
    
    ### node "group_membership"
    foreach my $v ( @{$contacts->{$id}{group_membership_full}} ) {
        my $org;
        foreach my $vv ( keys %{$v}) {
            if($v->{$vv}) {
                $org .= " " . $v->{$vv};
            }
        }
        $org =~ s/^[\t\f ]+|[\t\f ]+$//mg;
        
        if($org && $org ne "") {
            $org = custom_encode_string($org);
            
            my $p = $vcard->add_node( { 'node_type' => 'ORG', } );
            $p->name( $org );
        }
    }
    
    ### node "postal_address"
    foreach my $v ( @{$contacts->{$id}{"postal-address_v2_full"}} ) {
        my $org;
        foreach my $vv ( keys %{$v}) {
            if($v->{$vv}) {
                $org .= " " . $v->{$vv};
            }
        }
        $org =~ s/^[\t\f ]+|[\t\f ]+$//mg;
        
        if($org && $org ne "") {
            $org = custom_encode_string($org);
            
            my $p = $vcard->add_node( { 'node_type' => 'ADR', } );
            $p->city( $org );
        }
    }
    
    # delete loaded fields from hash
    # for easy visual debug
    
    delete( $contacts->{$id}{fullname} );
    
    delete( $contacts->{$id}{name} );
    delete( $contacts->{$id}{note} );
    delete( $contacts->{$id}{photo} );
    delete( $contacts->{$id}{group_membership} ); 
    delete( $contacts->{$id}{"postal-address_v2"} ); 
    delete( $contacts->{$id}{organization} );   
    delete( $contacts->{$id}{phone_v2} );
    delete( $contacts->{$id}{email_v2} );
    delete( $contacts->{$id}{contact_event} );
    delete( $contacts->{$id}{"vnd.com.whatsapp.profile"} );
    
    delete( $contacts->{$id}{name_full} );
    delete( $contacts->{$id}{note_full} );
    delete( $contacts->{$id}{photo_full} );
    delete( $contacts->{$id}{group_membership_full} );    
    delete( $contacts->{$id}{"postal-address_v2_full"} ); 
    delete( $contacts->{$id}{organization_full} );    
    delete( $contacts->{$id}{phone_v2_full} );
    delete( $contacts->{$id}{email_v2_full} );
    delete( $contacts->{$id}{contact_event_full} );
    delete( $contacts->{$id}{"vnd.com.whatsapp.profile_full"} );
}

# contacts count
#print Dumper scalar( keys $contacts);

# show unloaded fields
#print Dumper $contacts;

# export vcf
print $ab->export;

exit;

# ============================

sub custom_encode_string {
    
    # instead quote-printable
    
    my ($str) = @_;
    
    $str =~ s/[\r\n]/ /g;
    $str =~ s/,/\\,/g;
    
    #$str = MIME::QuotedPrint::encode($str);
    
    return $str;
}

sub load_contact_data {

    my $query = $db->prepare("SELECT d.raw_contact_id, m.mimetype, d.data1, d.data2, d.data3, d.data4 FROM data AS d LEFT JOIN mimetypes AS m WHERE d.mimetype_id=m._id");
    $query->execute() or die($db->errstr);
    
    while (@r = $query->fetchrow_array() ){
        my $type = $r[1];
        $type =~ s/vnd.android.cursor.item\///;
        
        my $data = $r[2];
        
        my $data_full = {
            'data1' => $r[2],
            'data2' => $r[3],
            'data3' => $r[4],
            'data4' => $r[5],
        };
        
        #$data = MIME::QuotedPrint::encode($data);
        #$data_full = MIME::QuotedPrint::decode($data_full);
    
        # by name or by id    
        my $fullname = $names->{$r[0]};
        #my $fullname = $r[0];
        
        push @{$contacts->{ $fullname }{$type}}, $data;
        push @{$contacts->{ $fullname }{"${type}_full"}}, $data_full;
    }  
}